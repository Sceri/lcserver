package ru.fablab.lcserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by Sceri 30.01.2017.
 */
@SpringBootApplication
@EnableScheduling
public class LCServerApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(LCServerApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(LCServerApplication.class, args);
    }

}
