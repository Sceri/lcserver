package ru.fablab.lcserver.service;

import ru.fablab.lcserver.user.User;
import ru.fablab.lcserver.web.dto.UserDTO;
import ru.fablab.lcserver.web.exception.UserAlreadyExistException;

import java.util.List;

/**
 * Created by Sceri 31.01.2017.
 */
public interface IUserService {

    User registerUser(UserDTO accountDto) throws UserAlreadyExistException;

    void saveRegisteredUser(User user);

    void deleteUser(User user);

    User findUserByEmail(String email);

    User getUserByID(long id);

    List<String> getUsersFromSessionRegistry();

    void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(User user, String password);

}
