package ru.fablab.lcserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.fablab.lcserver.security.ActiveUserStore;

/**
 * Created by Sceri 31.01.2017.
 */
@Configuration
public class ServerConfig {

    @Bean
    public ActiveUserStore activeUserStore() {
        return new ActiveUserStore();
    }

}
