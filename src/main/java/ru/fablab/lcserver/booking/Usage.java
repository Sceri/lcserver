package ru.fablab.lcserver.booking;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.springframework.format.annotation.DateTimeFormat;
import ru.fablab.lcserver.user.User;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sceri 02.02.2017.
 */
@Entity
@Table(name = "usages")
public class Usage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date start;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date end;

    public Usage() {

    }

    public Usage(User user) {
        this(user, new Date(), null);

    }

    public Usage(User user, Date start, Date end) {
        this.user = user;
        this.start = start;
        this.end = end;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int end() {
        setEnd(new Date());
        return calcTime();
    }

    public int calcTime() {
        if(start != null && end != null) {
            return (int) Math.ceil((end.getTime() - start.getTime()) / (60D * 1000D));
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Booking)) return false;
        Booking booking = (Booking) o;
        return Objects.equal(getUser(), booking.getUser()) &&
                Objects.equal(getStart(), booking.getStart()) &&
                Objects.equal(getEnd(), booking.getEnd());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUser(), getStart(), getEnd());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("user", user)
                .add("start", start)
                .add("end", end)
                .toString();
    }

}
