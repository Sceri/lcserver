package ru.fablab.lcserver.booking.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import ru.fablab.lcserver.booking.Booking;
import ru.fablab.lcserver.user.User;

import java.util.Date;
import java.util.List;

/**
 * Created by Sceri 02.02.2017.
 */
public interface BookingRepository extends JpaRepository<Booking, Long> {

    public List<Booking> findByUser(User user);

    @Query("SELECT b FROM Booking b WHERE b.end >= :date and b.user = :user")
    public List<Booking> findByUserAndNow(@Param("user") User user, @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @Param("date") Date date);

    @Query("SELECT b FROM Booking b WHERE b.start >= :fDate and b.end <= :sDate")
    public List<Booking> findByDate(@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @Param("fDate") Date fDate,
                                    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @Param("sDate") Date sDate);

    @Query("SELECT b FROM Booking b WHERE b.end >= :date")
    public List<Booking> findByNow(@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @Param("date") Date date);

    @Override
    public void delete(Booking booking);

}
