package ru.fablab.lcserver.booking.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import ru.fablab.lcserver.booking.Booking;
import ru.fablab.lcserver.booking.Usage;

import java.util.Date;
import java.util.List;

/**
 * Created by sceri on 04.02.17.
 */
public interface UsageRepository  extends JpaRepository<Usage, Long> {

    @Override
    public void delete(Usage booking);

    @Query("SELECT u FROM Usage u WHERE u.start >= :fDate and u.end <= :sDate")
    public List<Usage> findByDate(@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @Param("fDate") Date fDate,
                                    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @Param("sDate") Date sDate);

}
