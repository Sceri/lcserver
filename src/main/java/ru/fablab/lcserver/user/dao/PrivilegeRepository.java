package ru.fablab.lcserver.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.fablab.lcserver.user.Privilege;

/**
 * Created by Sceri 31.01.2017.
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

    public Privilege findByName(String name);

    @Override
    public void delete(Privilege privilege);

}
