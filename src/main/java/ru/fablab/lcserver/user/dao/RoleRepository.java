package ru.fablab.lcserver.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.fablab.lcserver.user.Role;

/**
 * Created by Sceri 31.01.2017.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    public Role findByName(String name);

    @Override
    public void delete(Role role);

}
