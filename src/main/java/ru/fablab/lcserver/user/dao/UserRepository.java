package ru.fablab.lcserver.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.fablab.lcserver.user.User;

/**
 * Created by Sceri 31.01.2017.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    public User findByEmail(String email);

    public User findByUuid(String uuid);

    @Override
    public void delete(User user);

}
