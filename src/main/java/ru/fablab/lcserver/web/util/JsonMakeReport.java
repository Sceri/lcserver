package ru.fablab.lcserver.web.util;

/**
 * Created by Sceri 05.02.2017.
 */
public class JsonMakeReport {

    private String start;
    private String end;

    public JsonMakeReport() {
    }

    public JsonMakeReport(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
