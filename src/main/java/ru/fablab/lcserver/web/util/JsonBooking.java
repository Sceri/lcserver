package ru.fablab.lcserver.web.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Sceri 03.02.2017.
 */
public class JsonBooking {
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    private Long id;
    private String user;
    private String from;
    private String to;

    public JsonBooking() {

    }

    public JsonBooking(Long id, String user, Date from, Date to) {
        this.id = id;
        this.user = user;
        this.from = DATE_FORMAT.format(from);
        this.to = DATE_FORMAT.format(to);
    }

    public JsonBooking(Date from, Date to) {
        this.from = DATE_FORMAT.format(from);
        this.to = DATE_FORMAT.format(to);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
