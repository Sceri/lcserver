package ru.fablab.lcserver.web.util.pickadate;

import java.util.Date;

/**
 * Created by Sceri 02.02.2017.
 */
public class JsonBookingPT {

    private int[] from;
    private int[] to;

    public JsonBookingPT(Date from, Date to) {
        this.from = new int[] { from.getHours(), from.getMinutes()};
        this.to = new int[] { to.getHours(), to.getMinutes()};
    }

    public int[] getFrom() {
        return from;
    }

    public void setFrom(int[] from) {
        this.from = from;
    }

    public int[] getTo() {
        return to;
    }

    public void setTo(int[] to) {
        this.to = to;
    }
}
