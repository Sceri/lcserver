package ru.fablab.lcserver.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.fablab.lcserver.booking.Usage;
import ru.fablab.lcserver.booking.dao.BookingRepository;
import ru.fablab.lcserver.security.LCUserDetails;
import ru.fablab.lcserver.user.User;
import ru.fablab.lcserver.user.dao.UserRepository;
import ru.fablab.lcserver.web.util.JsonBooking;

import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by Sceri 03.02.2017.
 */
@Controller
public class HomeController {

    private final UserRepository userRepository;
    private final BookingRepository bookingRepository;

    @Autowired
    public HomeController(UserRepository userRepository, BookingRepository bookingRepository) {
        this.userRepository = userRepository;
        this.bookingRepository = bookingRepository;
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView home() {
        User user = userRepository.findByEmail(getUserDetails().getUsername());

        ModelAndView mav = new ModelAndView();
        mav.addObject("firstName", user.getFirstName());
        mav.addObject("lastName", user.getLastName());
        mav.addObject("email", user.getEmail());
        mav.addObject("balance", user.getBalance());
        mav.addObject("time", user.getUsages().stream().mapToInt(Usage::calcTime).reduce((a, b) -> a + b).orElse(0));
        mav.setViewName("home");
        return mav;
    }

    private LCUserDetails getUserDetails() {
        return ((LCUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

}
