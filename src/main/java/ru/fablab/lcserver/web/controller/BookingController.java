package ru.fablab.lcserver.web.controller;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.fablab.lcserver.booking.Booking;
import ru.fablab.lcserver.booking.Usage;
import ru.fablab.lcserver.booking.dao.BookingRepository;
import ru.fablab.lcserver.security.LCUserDetails;
import ru.fablab.lcserver.user.User;
import ru.fablab.lcserver.user.dao.UserRepository;
import ru.fablab.lcserver.web.dto.DateDTO;
import ru.fablab.lcserver.web.exception.NotEnoughMoneyException;
import ru.fablab.lcserver.web.util.GenericResponse;
import ru.fablab.lcserver.web.util.JsonBooking;
import ru.fablab.lcserver.web.util.pickadate.JsonBookingPT;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Sceri 02.02.2017.
 */
@Controller
public class BookingController {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat timeFormat = new SimpleDateFormat("HH:mm");

    private final UserRepository userRepository;
    private final BookingRepository bookingRepository;

    @Autowired
    public BookingController(UserRepository userRepository, BookingRepository bookingRepository) {
        this.userRepository = userRepository;
        this.bookingRepository = bookingRepository;
    }

    @RequestMapping(value = "/booking", method = RequestMethod.GET)
    public ModelAndView booking() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("bookings", bookingRepository.findByNow(new Date()).stream().map(booking -> new JsonBooking(booking.getId(), booking.getUser().getEmail(), booking.getStart(), booking.getEnd())).collect(Collectors.toList()));
        mav.setViewName("booking");
        return mav;
    }

    @RequestMapping(value = "/booking/pickdate", method = RequestMethod.POST)
    @ResponseBody
    public List<JsonBookingPT> booking(@Valid final DateDTO dateDTO) {
        if(dateDTO.getDate() == null) {
            return null;
        }

        try {
            Date selectedDate = dateFormat.parse(dateDTO.getDate());

            Calendar c = Calendar.getInstance();
            c.setTime(selectedDate);
            c.add(Calendar.DATE, 1);
            Date incDate = c.getTime();

            return bookingRepository.findByDate(selectedDate, incDate).stream().map(booking -> new JsonBookingPT(booking.getStart(), booking.getEnd())).collect(Collectors.toList());
        } catch (ParseException e) {
            return null;
        }
    }

    @Transactional
    @RequestMapping(value = "/booking/process", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse process(@Valid final DateDTO dateDTO) {
        Date start = generateDate(dateDTO.getDate(), dateDTO.getTimeStart());
        Date end = generateDate(dateDTO.getDate(), dateDTO.getTimeEnd());
        if(start != null && end != null) {
            User user = userRepository.findByEmail(getUserDetails().getUsername());

            int diff = (int)(end.getTime() - start.getTime()) / (60 * 1000);
            if(user.getBalance() >= diff) {
                Booking booking = new Booking(user, start, end);

                user.setBalance(user.getBalance() - diff);
                user.getBookings().add(booking);
                userRepository.save(user);
                return new GenericResponse("success");
            }

            throw new NotEnoughMoneyException("Недостаточно денег на балансе");
        }

        throw new IllegalStateException("Ooops");
    }

    private LCUserDetails getUserDetails() {
        return ((LCUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    private Date generateDate(String date, String time) {
        try {
            Date start = dateFormat.parse(date);
            Date dateTime = timeFormat.parse(time);
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateTime);
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE);
            cal.setTime(start);
            cal.add(Calendar.HOUR_OF_DAY, hour);
            cal.add(Calendar.MINUTE, minute);
            return cal.getTime();
        } catch (ParseException e) {
            return null;
        }
    }

    @RequestMapping(value = "/booking/makeReport", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse response) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Расписание");

        int startRowIndex = 0;
        int startColIndex = 0;

        buildReport(sheet, startRowIndex, startColIndex);
        fillReport(sheet, startRowIndex, startColIndex, bookingRepository.findByNow(new Date()));

        String fileName = "schedule.xls";
        response.setHeader("Content-Disposition", "inline; filename=" + fileName);
        response.setContentType("application/vnd.ms-excel");

        try {
            ServletOutputStream outputStream = response.getOutputStream();
            sheet.getWorkbook().write(outputStream);
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void buildReport(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
        worksheet.setColumnWidth(0, 5000);
        worksheet.setColumnWidth(1, 5000);
        worksheet.setColumnWidth(2, 5000);
        worksheet.setColumnWidth(3, 5000);
        worksheet.setColumnWidth(4, 5000);
        worksheet.setColumnWidth(5, 5000);

        buildTitle(worksheet, startRowIndex, startColIndex);
        buildHeaders(worksheet, startRowIndex, startColIndex);
    }

    private static void buildTitle(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
        Font fontTitle = worksheet.getWorkbook().createFont();
        fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
        fontTitle.setFontHeight((short) 280);

        HSSFCellStyle cellStyleTitle = worksheet.getWorkbook().createCellStyle();
        cellStyleTitle.setAlignment(HorizontalAlignment.CENTER);
        cellStyleTitle.setWrapText(true);
        cellStyleTitle.setFont(fontTitle);

        HSSFRow rowTitle = worksheet.createRow((short) startRowIndex);
        rowTitle.setHeight((short) 500);
        HSSFCell cellTitle = rowTitle.createCell(startColIndex);
        cellTitle.setCellValue("Расписание");
        cellTitle.setCellStyle(cellStyleTitle);

        worksheet.addMergedRegion(new CellRangeAddress(0,0,0,5));
        worksheet.addMergedRegion(new CellRangeAddress(1,1,0,5));

        HSSFCellStyle dateCellStyle = worksheet.getWorkbook().createCellStyle();
        dateCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        dateCellStyle.setDataFormat(worksheet.getWorkbook().createDataFormat().getFormat("dd.MM.yyyy HH:mm"));

        HSSFRow dateTitle = worksheet.createRow((short) startRowIndex + 1);
        HSSFCell cellDate = dateTitle.createCell(startColIndex);
        cellDate.setCellStyle(dateCellStyle);
        cellDate.setCellValue("От: " + new Date());
    }

    private static void buildHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
        Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
        headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        headerCellStyle.setWrapText(true);
        headerCellStyle.setFont(font);
        headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);

        HSSFRow rowHeader = worksheet.createRow((short) startRowIndex + 2);
        rowHeader.setHeight((short) 500);

        HSSFCell cell1 = rowHeader.createCell(startColIndex);
        cell1.setCellValue("Id");
        cell1.setCellStyle(headerCellStyle);

        HSSFCell fio = rowHeader.createCell(startColIndex + 1);
        fio.setCellValue("ФИО");
        fio.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 1);

        HSSFCell cell2 = rowHeader.createCell(startColIndex + 2);
        cell2.setCellValue("EMail");
        cell2.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 2);

        HSSFCell bookingStart = rowHeader.createCell(startColIndex + 3);
        bookingStart.setCellValue("Начало");
        bookingStart.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 3);

        HSSFCell bookingEnd = rowHeader.createCell(startColIndex + 4);
        bookingEnd.setCellValue("Конец");
        bookingEnd.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 4);

        HSSFCell cell6 = rowHeader.createCell(startColIndex + 5);
        cell6.setCellValue("Время работы (мин)");
        cell6.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 5);
    }

    private static void fillReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<Booking> datasource) {
        startRowIndex += 2;

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        bodyCellStyle.setWrapText(true);

        HSSFCellStyle dateCellStyle = worksheet.getWorkbook().createCellStyle();
        dateCellStyle.cloneStyleFrom(bodyCellStyle);
        dateCellStyle.setDataFormat(worksheet.getWorkbook().createDataFormat().getFormat("dd.MM.yyyy HH:mm"));

        for (int i = startRowIndex; i + startRowIndex - 2 < datasource.size() + 2; i++) {
            HSSFRow row = worksheet.createRow((short) i + 1);

            HSSFCell cell1 = row.createCell(startColIndex);
            cell1.setCellValue(datasource.get(i - 2).getId());
            cell1.setCellStyle(bodyCellStyle);

            HSSFCell fio = row.createCell(startColIndex + 1);
            User user = datasource.get(i - 2).getUser();
            fio.setCellValue(user.getFirstName() + " " + user.getLastName());
            fio.setCellStyle(bodyCellStyle);
            worksheet.autoSizeColumn(startColIndex + 1);

            HSSFCell cell2 = row.createCell(startColIndex + 2);
            cell2.setCellValue(datasource.get(i - 2).getUser().getEmail());
            cell2.setCellStyle(bodyCellStyle);
            worksheet.autoSizeColumn(startColIndex + 2);

            HSSFCell bookingStart = row.createCell(startColIndex + 3);
            bookingStart.setCellValue(datasource.get(i - 2).getStart());
            bookingStart.setCellStyle(dateCellStyle);
            worksheet.autoSizeColumn(startColIndex + 3);

            HSSFCell bookingEnd = row.createCell(startColIndex + 4);
            bookingEnd.setCellValue(datasource.get(i - 2).getEnd());
            bookingEnd.setCellStyle(dateCellStyle);
            worksheet.autoSizeColumn(startColIndex + 4);

            HSSFCell cell6 = row.createCell(startColIndex + 5);
            cell6.setCellValue(datasource.get(i - 2).calcTime());
            cell6.setCellStyle(bodyCellStyle);
            worksheet.autoSizeColumn(startColIndex + 5);
        }
    }
}
