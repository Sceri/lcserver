package ru.fablab.lcserver.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.fablab.lcserver.booking.Booking;
import ru.fablab.lcserver.booking.Usage;
import ru.fablab.lcserver.booking.dao.BookingRepository;
import ru.fablab.lcserver.user.User;
import ru.fablab.lcserver.user.dao.UserRepository;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * Created by Sceri 03.02.2017.
 */
@RestController
public class ArduinoController {
    private static final String NO = Character.toString((char)0);
    private static final String YES = Character.toString((char)1);

    private final UserRepository userRepository;

    @Autowired
    public ArduinoController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/arduino/start", method = { RequestMethod.GET, RequestMethod.POST })
    public String start(@RequestParam(name = "uuid") String uuid) {
        User user = userRepository.findByUuid(uuid);
        if(user == null) {
            return NO;
        }

        if(user.getBalance() > 0) {
            user.getUsages().add(new Usage(user));
            userRepository.save(user);
            return YES;
        }

        return NO;
    }

    @RequestMapping(value = "/arduino/stop", method = { RequestMethod.GET, RequestMethod.POST })
    @Transactional
    public String stop(@RequestParam(name = "uuid") String uuid) {
        User user = userRepository.findByUuid(uuid);
        if(user == null) {
            return NO;
        }

        Usage usage = findLastNull(user);
        if(usage != null) {
            int cost = usage.end();
            user.setBalance(user.getBalance() - cost);
            userRepository.save(user);
            return YES;
        }

        return NO;
    }

    private Usage findLastNull(User user) {
        for(Usage usage : user.getUsages()) {
            if(usage.getEnd() == null) {
                return usage;
            }

        }
        return null;
    }
}
