package ru.fablab.lcserver.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.fablab.lcserver.security.LCUserDetails;
import ru.fablab.lcserver.service.IUserService;
import ru.fablab.lcserver.user.User;
import ru.fablab.lcserver.web.dto.UserDTO;
import ru.fablab.lcserver.web.exception.InvalidOldPasswordException;
import ru.fablab.lcserver.web.util.GenericResponse;
import ru.fablab.lcserver.web.util.PasswordDTO;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

/**
 * Created by Sceri 31.01.2017.
 */
@Controller
public class UserController {
    private final MessageSource messages;
    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService, MessageSource messages) {
        this.userService = userService;
        this.messages = messages;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView registerUserAccount() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return new ModelAndView("forward:/home.html");
        }

        return new ModelAndView("login");
    }

    @RequestMapping(value = "/user/registration", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse registerUserAccount(@Valid final UserDTO accountDto, final HttpServletRequest request) {
        userService.registerUser(accountDto);
        return new GenericResponse("success");
    }

    @RequestMapping(value = "/user/updatePassword", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse changeUserPassword(final Locale locale, @Valid PasswordDTO passwordDto) {
        final User user = userService.findUserByEmail(getUserDetails().getUsername());

        if (!userService.checkIfValidOldPassword(user, passwordDto.getOldPassword())) {
            throw new InvalidOldPasswordException();
        }
        userService.changeUserPassword(user, passwordDto.getNewPassword());
        return new GenericResponse(messages.getMessage("message.updatePasswordSuc", null, locale));
    }

    private LCUserDetails getUserDetails() {
        return ((LCUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

}
