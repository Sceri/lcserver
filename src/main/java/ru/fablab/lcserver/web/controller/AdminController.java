package ru.fablab.lcserver.web.controller;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.fablab.lcserver.booking.Booking;
import ru.fablab.lcserver.booking.Usage;
import ru.fablab.lcserver.booking.dao.BookingRepository;
import ru.fablab.lcserver.booking.dao.UsageRepository;
import ru.fablab.lcserver.user.User;
import ru.fablab.lcserver.user.dao.UserRepository;
import ru.fablab.lcserver.web.util.GenericResponse;
import ru.fablab.lcserver.web.util.JsonBooking;
import ru.fablab.lcserver.web.util.JsonMakeReport;
import ru.fablab.lcserver.web.util.JsonUser;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Sceri 03.02.2017.
 */
@Controller
public class AdminController {
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private final UserRepository userRepository;
    private final UsageRepository usageRepository;

    @Autowired
    public AdminController(UserRepository userRepository, UsageRepository usageRepository) {
        this.userRepository = userRepository;
        this.usageRepository = usageRepository;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView admin(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("users", userRepository.findAll().stream().map(user -> new JsonUser(user.getId(), user.getUuid(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getBalance())).collect(Collectors.toList()));
        mav.setViewName("admin");
        return mav;
    }

    @RequestMapping(value = "/admin/change", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse change(@Valid JsonUser jsonUser, HttpServletRequest request) {
        User user = userRepository.findOne(jsonUser.getId());
        user.setUuid(jsonUser.getUuid());
        user.setFirstName(jsonUser.getFirstName());
        user.setLastName(jsonUser.getLastName());
        user.setEmail(jsonUser.getEmail());
        user.setBalance(jsonUser.getBalance());
        userRepository.save(user);
        return new GenericResponse("success");
    }

    @RequestMapping(value = "/admin/makeReport", method = RequestMethod.GET)
    public void downloadFile(@Valid JsonMakeReport makeReport, HttpServletResponse response) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Отчёт");

        int startRowIndex = 0;
        int startColIndex = 0;

        buildReport(sheet, startRowIndex, startColIndex, makeReport.getStart(), makeReport.getEnd());
        try {
            fillReport(sheet, startRowIndex, startColIndex, usageRepository.findByDate(DATE_FORMAT.parse(makeReport.getStart()), DATE_FORMAT.parse(makeReport.getEnd())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String fileName = "report.xls";
        response.setHeader("Content-Disposition", "inline; filename=" + fileName);
        response.setContentType("application/vnd.ms-excel");

        try {
            ServletOutputStream outputStream = response.getOutputStream();
            sheet.getWorkbook().write(outputStream);
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void buildReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, String start, String end) {
        worksheet.setColumnWidth(0, 5000);
        worksheet.setColumnWidth(1, 5000);
        worksheet.setColumnWidth(2, 5000);
        worksheet.setColumnWidth(3, 5000);
        worksheet.setColumnWidth(4, 5000);
        worksheet.setColumnWidth(5, 5000);
        worksheet.setColumnWidth(6, 5000);
        worksheet.setColumnWidth(7, 5000);

        buildTitle(worksheet, startRowIndex, startColIndex, start, end);
        buildHeaders(worksheet, startRowIndex, startColIndex);
    }

    private static void buildTitle(HSSFSheet worksheet, int startRowIndex, int startColIndex, String start, String end) {
        Font fontTitle = worksheet.getWorkbook().createFont();
        fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
        fontTitle.setFontHeight((short) 280);

        HSSFCellStyle cellStyleTitle = worksheet.getWorkbook().createCellStyle();
        cellStyleTitle.setAlignment(HorizontalAlignment.CENTER);
        cellStyleTitle.setWrapText(true);
        cellStyleTitle.setFont(fontTitle);

        HSSFRow rowTitle = worksheet.createRow((short) startRowIndex);
        rowTitle.setHeight((short) 500);
        HSSFCell cellTitle = rowTitle.createCell(startColIndex);
        cellTitle.setCellValue("Отчёт по использованию лазера");
        cellTitle.setCellStyle(cellStyleTitle);

        worksheet.addMergedRegion(new CellRangeAddress(0,0,0,7));
        worksheet.addMergedRegion(new CellRangeAddress(1,1,0,7));

        HSSFCellStyle dateCellStyle = worksheet.getWorkbook().createCellStyle();
        dateCellStyle.setAlignment(CellStyle.ALIGN_CENTER);

        HSSFRow dateTitle = worksheet.createRow((short) startRowIndex + 1);
        HSSFCell cellDate = dateTitle.createCell(startColIndex);
        cellDate.setCellStyle(dateCellStyle);
        cellDate.setCellValue(String.format("От: %s. До: %s.", start, end));
    }

    private static void buildHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
        Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
        headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        headerCellStyle.setWrapText(true);
        headerCellStyle.setFont(font);
        headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);

        HSSFRow rowHeader = worksheet.createRow((short) startRowIndex + 2);
        rowHeader.setHeight((short) 500);

        HSSFCell cell1 = rowHeader.createCell(startColIndex);
        cell1.setCellValue("Id");
        cell1.setCellStyle(headerCellStyle);

        HSSFCell fio = rowHeader.createCell(startColIndex + 1);
        fio.setCellValue("ФИО");
        fio.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 1);

        HSSFCell cell2 = rowHeader.createCell(startColIndex + 2);
        cell2.setCellValue("EMail");
        cell2.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 2);

        HSSFCell cell3 = rowHeader.createCell(startColIndex + 3);
        cell3.setCellValue("UID");
        cell3.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 3);

        HSSFCell bookingStart = rowHeader.createCell(startColIndex + 4);
        bookingStart.setCellValue("Начало");
        bookingStart.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 4);

        HSSFCell bookingEnd = rowHeader.createCell(startColIndex + 5);
        bookingEnd.setCellValue("Конец");
        bookingEnd.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 5);

        HSSFCell cell6 = rowHeader.createCell(startColIndex + 6);
        cell6.setCellValue("Время работы (мин)");
        cell6.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 6);

        HSSFCell cell7 = rowHeader.createCell(startColIndex + 7);
        cell7.setCellValue("Стоимость");
        cell7.setCellStyle(headerCellStyle);
        worksheet.autoSizeColumn(startColIndex + 7);
    }

    private static void fillReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<Usage> datasource) {
        startRowIndex += 2;

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        bodyCellStyle.setWrapText(true);

        HSSFCellStyle dateCellStyle = worksheet.getWorkbook().createCellStyle();
        dateCellStyle.cloneStyleFrom(bodyCellStyle);
        dateCellStyle.setDataFormat(worksheet.getWorkbook().createDataFormat().getFormat("dd.MM.yyyy HH:mm"));

        for (int i = startRowIndex; i + startRowIndex - 2 < datasource.size() + 2; i++) {
            HSSFRow row = worksheet.createRow((short) i + 1);

            HSSFCell cell1 = row.createCell(startColIndex);
            cell1.setCellValue(datasource.get(i - 2).getId());
            cell1.setCellStyle(bodyCellStyle);

            HSSFCell fio = row.createCell(startColIndex + 1);
            User user = datasource.get(i - 2).getUser();
            fio.setCellValue(user.getFirstName() + " " + user.getLastName());
            fio.setCellStyle(bodyCellStyle);
            worksheet.autoSizeColumn(startColIndex + 1);

            HSSFCell cell2 = row.createCell(startColIndex + 2);
            cell2.setCellValue(datasource.get(i - 2).getUser().getEmail());
            cell2.setCellStyle(bodyCellStyle);
            worksheet.autoSizeColumn(startColIndex + 2);

            HSSFCell cell3 = row.createCell(startColIndex + 3);
            cell3.setCellValue(datasource.get(i - 2).getUser().getUuid());
            cell3.setCellStyle(bodyCellStyle);
            worksheet.autoSizeColumn(startColIndex + 3);

            HSSFCell bookingStart = row.createCell(startColIndex + 4);
            bookingStart.setCellValue(datasource.get(i - 2).getStart());
            bookingStart.setCellStyle(dateCellStyle);
            worksheet.autoSizeColumn(startColIndex + 4);

            HSSFCell bookingEnd = row.createCell(startColIndex + 5);
            bookingEnd.setCellValue(datasource.get(i - 2).getEnd());
            bookingEnd.setCellStyle(dateCellStyle);
            worksheet.autoSizeColumn(startColIndex + 5);

            HSSFCell cell6 = row.createCell(startColIndex + 6);
            cell6.setCellValue(datasource.get(i - 2).calcTime());
            cell6.setCellStyle(bodyCellStyle);
            worksheet.autoSizeColumn(startColIndex + 6);

            HSSFCell cell7 = row.createCell(startColIndex + 7);
            cell7.setCellValue(datasource.get(i - 2).calcTime());
            cell7.setCellStyle(bodyCellStyle);
            worksheet.autoSizeColumn(startColIndex + 7);
        }
    }

}
