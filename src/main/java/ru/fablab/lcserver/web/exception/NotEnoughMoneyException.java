package ru.fablab.lcserver.web.exception;

/**
 * Created by Sceri 03.02.2017.
 */
public class NotEnoughMoneyException extends RuntimeException {

    public NotEnoughMoneyException() {
        super();
    }

    public NotEnoughMoneyException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public NotEnoughMoneyException(final String message) {
        super(message);
    }

    public NotEnoughMoneyException(final Throwable cause) {
        super(cause);
    }

}
