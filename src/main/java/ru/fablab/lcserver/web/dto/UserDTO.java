package ru.fablab.lcserver.web.dto;

import com.google.common.base.MoreObjects;
import ru.fablab.lcserver.web.validation.PasswordMatches;
import ru.fablab.lcserver.web.validation.ValidEmail;
import ru.fablab.lcserver.web.validation.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 * Created by Sceri 31.01.2017.
 */
@PasswordMatches
public class UserDTO {
    @NotNull
    @Size(min = 1)
    private String firstName;

    @NotNull
    @Size(min = 1)
    private String lastName;

    @ValidPassword
    private String password;

    @NotNull
    @Size(min = 1)
    private String matchingPassword;

    @ValidEmail
    @NotNull
    @Size(min = 1)
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    private int role;

    public int getRole() {
        return role;
    }

    public void setRole(final int role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(final String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("password", password)
                .add("matchingPassword", matchingPassword)
                .add("email", email)
                .add("role", role)
                .toString();
    }
}
