package ru.fablab.lcserver.web.dto;

import ru.fablab.lcserver.booking.Booking;

import java.util.List;

/**
 * Created by Sceri 02.02.2017.
 */
public class BookingDTO {

    private List<Booking> bookings;

    public BookingDTO(List<Booking> bookings) {
        this.bookings = bookings;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}
