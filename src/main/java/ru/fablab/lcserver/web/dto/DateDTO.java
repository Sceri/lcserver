package ru.fablab.lcserver.web.dto;

/**
 * Created by Sceri 02.02.2017.
 */
public class DateDTO {

    private String date;
    private String timeStart;
    private String timeEnd;

    public DateDTO() {
    }

    public DateDTO(String date, String timeStart, String timeEnd) {
        this.date = date;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }
}
