package ru.fablab.lcserver.security;

import com.google.common.base.*;
import com.google.common.base.Objects;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;
import ru.fablab.lcserver.user.User;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Sceri 03.02.2017.
 */
public class LCUserDetails implements UserDetails, CredentialsContainer {

    private final String email;
    private final String uuid;
    private String password;
    private final Set<GrantedAuthority> authorities;
    private int balance;

    public LCUserDetails(String email, String uuid, String password, Collection<? extends GrantedAuthority> authorities, int balance) {
        if(email != null && !"".equals(email) && password != null) {
            this.email = email;
            this.uuid = uuid;
            this.password = password;
            this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
            this.balance = balance;
        } else {
            throw new IllegalArgumentException("Cannot pass null or empty values to constructor");
        }
    }

    @Override
    public void eraseCredentials() {
        password = null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getUuid() {
        return uuid;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LCUserDetails)) return false;
        LCUserDetails that = (LCUserDetails) o;
        return com.google.common.base.Objects.equal(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(email);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("email", email)
                .add("password", "[PROTECTED]")
                .add("authorities", authorities)
                .toString();
    }

    private static SortedSet<GrantedAuthority> sortAuthorities(Collection<? extends GrantedAuthority> authorities) {
        Assert.notNull(authorities, "Cannot pass a null GrantedAuthority collection");
        TreeSet<GrantedAuthority> sortedAuthorities = new TreeSet<>(new AuthorityComparator());
        for (GrantedAuthority grantedAuthority : authorities) {
            Assert.notNull(grantedAuthority, "GrantedAuthority list cannot contain any null elements");
            sortedAuthorities.add(grantedAuthority);
        }
        return sortedAuthorities;
    }

    private static class AuthorityComparator implements Comparator<GrantedAuthority>, Serializable {
        public int compare(GrantedAuthority g1, GrantedAuthority g2) {
            return g2.getAuthority() == null ? -1 : (g1.getAuthority() == null ? 1 : g1.getAuthority().compareTo(g2.getAuthority()));
        }
    }
}
