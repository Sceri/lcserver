package ru.fablab.lcserver.security;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sceri 31.01.2017.
 */
public class ActiveUserStore {

    public List<String> users;

    public ActiveUserStore() {
        users = new ArrayList<String>();
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

}
